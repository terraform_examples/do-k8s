provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_kubernetes_cluster" "cluster" {
  name = "${var.name}-${var.region}-${var.env}"
  region = "${var.region}"
  version = "${var.version}"
  tags = [
    "${var.name}",
    "${var.region}",
    "${var.env}"
  ]

  node_pool {
    name = "${var.name}-${var.region}-${var.env}-pool"
    size = "${var.vm_size}"
    node_count = "${var.vm_count}"
  }
}

output "name" { value = "${var.name}-${var.region}-${var.env}" }

output "client_certificate" {
    value = "${base64decode(digitalocean_kubernetes_cluster.cluster.kube_config.0.client_certificate)}"
}
output "client_key" {
    value = "${base64decode(digitalocean_kubernetes_cluster.cluster.kube_config.0.client_key)}"
}
output "cluster_ca_certificate" {
    value = "${base64decode(digitalocean_kubernetes_cluster.cluster.kube_config.0.cluster_ca_certificate)}"
}
