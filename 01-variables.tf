variable "name" { default = "clubinclub" }
variable "region" { default = "nyc1" }
variable "env" { default = "prod" }
variable "version" { default = "1.12.1-do.2" }
variable "do_token" { }

variable "vm_size" { default = "s-2vcpu-2gb" }
variable "vm_count" { default=2 }
