provider "helm" {
  install_tiller = true
  namespace = "kube-system"
  service_account = "tiller"

  kubernetes {
    host = "${digitalocean_kubernetes_cluster.cluster.endpoint}"
    client_certificate = "${base64decode(digitalocean_kubernetes_cluster.cluster.kube_config.0.client_certificate)}"
    client_key = "${base64decode(digitalocean_kubernetes_cluster.cluster.kube_config.0.client_key)}"
    cluster_ca_certificate = "${base64decode(digitalocean_kubernetes_cluster.cluster.kube_config.0.cluster_ca_certificate)}"
  }
}

resource "helm_release" "nginx_ingress" {
  name = "nginx-ingress"
  chart = "stable/nginx-ingress"
  namespace = "kube-system"
}
