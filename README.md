# k8s over digitalocean

### use

    DO_TOKEN= make

### details

- 00-terraform.tf
  - (optional file)
  - host with [REST](https://github.com/egeneralov/terraform-state-backend.git) service for save state
  - replace `clubinclub-nyc1-prod` with your uniq value per deployment

- 01-values.tf
  - use digitalocean's slug's for `region` and `version`

- 04-helm.tf
  - example usage for helm install chart
