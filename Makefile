all:
	terraform init
	terraform apply -auto-approve -var do_token=${DO_TOKEN}

clean:
	terraform destroy -auto-approve -var do_token=${DO_TOKEN}
	cat .gitignore | xargs rm -rf  

